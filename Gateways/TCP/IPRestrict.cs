﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore
{
    static class IPRestrict
    {
        internal static StatusCodes RestrictWebApp(string ip)
        {
            if (Sys.tcpsrv.WebAppIP != ip)
            {
                Sys.tcpsrv.WebAppIP = ip;
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorIPDuplicate;
        }

        internal static StatusCodes RestrictHttpApi(string ip)
        {
            if (Sys.tcpsrv.HttpApiIP != ip)
            {
                Sys.tcpsrv.HttpApiIP = ip;
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorIPDuplicate;
        }

        internal static StatusCodes RestrictDaemon(string ip)
        {
            if (Sys.tcpsrv.DaemonIP != ip)
            {
                Sys.tcpsrv.DaemonIP = ip;
                return StatusCodes.Success;
            }
            else return StatusCodes.ErrorIPDuplicate;
        }
    }
}
